import {
  pipeline,
  Readable,
  Transform,
  TransformCallback,
  Writable,
} from 'stream';

const testPipeline = () => {
  const start = Date.now();
  let counter = 1;

  const source = new Readable({
    read() {
      console.log('SOURCE ----------------->>>');
      console.log('Reading:', counter, Date.now() - start);

      const result = this.push(counter++);

      console.log('Result:', result);
      console.log('Read-Buffer:', (this as any)._readableState.length);
      console.log('SOURCE -----------------<<<\n');
    },

    // Storing items to be read by client / destination(s)
    objectMode: true,
    highWaterMark: 4,
  });

  const processor = new Transform({
    // Input: Writable-Internal-Buffer / Input-Internal-Buffer - Storing items to be transformed while
    //    1) Transformation is busy with the current item (Callback has not been called)
    //    2) Output-Internal-Buffer is full - Transformations are NOT running (Since there is nowhere to output results)
    // Output: Readable-Internal-Buffer / Output-Internal-Buffer - Storing transformed items output to be read
    //    - Reading by Clients (Manual API) or Destinations (Writable)
    // Flow: [Source] => Input-Internal-Buffer -> Transformation -> Output-Internal-Buffer => [Destinations]
    //    - Data is simply moved between Internal-Buffers (Input and Output)
    //    - Events are emitted ("data" / "close") when PUSHING to the Readable-Internal-Buffer (Just like with Readable)
    // IMPORTANT: Does NOT run if the Output-Internal-Buffer / Readable-Internal-Buffer is full (Nowhere to output data)
    transform(item: number, _: BufferEncoding, callback: TransformCallback) {
      console.log('PROCESSOR ----------------->>>');
      console.log('Transforming:', counter, Date.now() - start);
      console.log(
        'Buffers:',
        (this as any)._writableState.length,
        (this as any)._readableState.length,
      );

      callback(null, item); // Simply passing along

      console.log('Transformed:', counter, Date.now() - start);
      console.log(
        'Buffers:',
        (this as any)._writableState.length,
        (this as any)._readableState.length,
      );
      console.log('PROCESSOR -----------------<<<\n');
    },

    // Distinct settings for Input and Output ends -> Internal-Buffers and Storage-Formats (Raw / Object)
    // Storing items that need to be processed / transformed - Queueing for "_transform()"
    writableObjectMode: true,
    writableHighWaterMark: 2,
    // Storing items output from transforming - Queueing to be moved to Destination(s)
    readableObjectMode: true,
    readableHighWaterMark: 3,
    // Alternative: Same settings for Input and Output ends
    // objectMode: true,
    // highWaterMark: 3,
  });

  const destination = new Writable({
    write(item: number) {
      // Important: NOT calling callback (NEVER writing items) - Used for better debugging / information / buffering
      console.log('DESTINATION ----------------->>>');
      console.log('Writing:', item, Date.now() - start);
      console.log('DESTINATION -----------------<<<\n');
    },

    // Storing items to be written by the writer
    objectMode: true,
    highWaterMark: 2,
  });

  // Super-Important: Streams do NOT
  pipeline(source, processor, destination, () => {
    console.log('Finished');
  });

  // Printing the entire state
  setImmediate(() => {
    console.log('Source:', (source as any)._readableState.length);
    console.log('Processor-Input:', (processor as any)._writableState.length);
    console.log('Processor-Output:', (processor as any)._readableState.length);
    console.log('Destination:', (destination as any)._writableState.length);
  });
};

testPipeline();
