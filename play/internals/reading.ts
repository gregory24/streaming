import { Readable } from 'stream';

// TODO: Destroying

// TODO: Closed / Finished / Ended EVENTS

const TestReader = (start: number): Readable => {
  let counter = 0;

  return new Readable({
    // Reading items one-by-one -> Leaving it up for the consumers / destinations / subscribers to use them
    // Running when
    //   1) "read()" is called on the Readable
    //   2) "this.push(...)" was called with data and the Internal-Buffer can still be filled up
    //         - Calling "_read()" until the Internal-Buffer fills up completely
    read() {
      console.log('------------------------------->');
      console.log('Reading started');
      if (counter === 10) {
        this.push(null);
        console.log('Data ended');
      } else {
        // Important: No need to check result since items are pushed one-by-one
        //   -> "_read()" is ONLY called when there is space available in the Internal-Buffer
        //   => Can definitely fit in a single element
        const result = this.push(counter++);
        console.log('Read:', counter, result);
        console.log('Time:', Date.now() - start);
        console.log('Buffer:', (this as any)._readableState.length);
      }
      console.log('Reading finished');
      console.log('-------------------------------<');
    },

    objectMode: true,
    highWaterMark: 4,
  });
};

const TestReaderWithSizes = (start: number): Readable => {
  let counter = 0;

  return new Readable({
    // Size: Either using
    //    1) HWM - if no size is specified in "read(SIZE)" OR if SIZE is LESS than / EQUAL to HWM
    //    2) SIZE - if specified in "read(SIZE)" AND it is MORE than HWM
    //  Important: Does NOT handle Back-Pressure / Internal-Buffer Size with custom sizes - MUST check PUSH results
    read(size?: number) {
      console.log('------------------------------->');
      console.log('Performing reading', size);

      let outputSize = size || 1;
      let result: boolean;

      while (outputSize--) {
        // Finishing / Ending
        if (counter === 10) {
          console.log('Ending');

          this.push(null);

          break;
        }

        // Checking whether we can continue filling up Internal-Buffer - Impossible to implement for Readable internally
        //    -> Using Internal-Buffer status / size to determine whether more data can be pushed
        //    => Does NOT work when SIZE is specified
        result = this.push(counter++);

        if (!result) {
          break;
        }

        // Push: Uses Back-Pressure the same way Writable does with "write()"
        //    True: Can push more items - Internal-Buffer is NOT FULL
        //    False: More items SHOULD NOT be pushed - Internal-Buffer was FILLED UP
        //  -> Only useful / usable when 1) Pushing data to Readable externally from the outside
        //                               2) Using "size" to push multiple chunks / objects
        //  => Respecting Back-Pressure / Internal-Buffer
        console.log('Reading:', counter, result);
        console.log('Time:', Date.now() - start);
        console.log('Buffer:', (this as any)._readableState.length);
        console.log('-------------------------------<');
      }
    },

    objectMode: true,
    highWaterMark: 3, // Buffering up to 3 items internally before stopping reading
    //  - Does NOT matter whether in FLOWING or PAUSED mode
    //  -> Returning "false" from PUSH when the Internal-Buffer is filled up
  });
};

// Manually reading data from Readable -> Readable stops reading more data at some point
//    - Internal-Buffer gets filled up completely due to NOT getting cleared at the same speed
//    -> Items are only removed from the Internal-Buffer when they are READ explicitly
const testPausedMode = (reader: Readable) => {
  const readData = () => {
    console.log('------------------------------------------------------->');
    console.log('CALL STARTED');

    // Manually performing "read()" on the Readable does 2 things:
    //    1) Asking "_read()" to be run -> Ensures that there is data in the Internal-Buffer at all times if possible
    //         - Invoked EVEN WHEN THE INTERNAL-BUFFER IS FULL (Reached it's High-Water-Mark)
    //         - Passing SIZE to the "_read()" to read a specific number of items / chunks (Using HWM by default)
    //    2) Taking a single item from the Internal-Buffer -> Removing it => Returning NULL when there is no data in it
    const data = reader.read();
    // const data = reader.read(4);

    console.log('DATA:', data);

    if (!reader.readableEnded) {
      setTimeout(readData, 100);
    }

    console.log('CALL ENDED');
    console.log('-------------------------------------------------------<');
  };

  // readData();
};

// Automatically reading data from Readable when "data" EVENTS are emitted
//    - Internal-Buffer is cleared automatically after each event is emitted
//    -> Items are CONSTANTLY removed form the Internal-Buffer after EACH event is emitted
//    => Internal-Buffer almost always remains empty / half-full
const testFlowingMode = (reader: Readable) => {
  // Important: AUTOMATICALLY switching Readable to "FLOWING" mode when a "data" EVENT-HANDLER is subscribed
  //    - No need to switch to manually using "resume()"
  //    -> NOT WAITING FOR Event-Handlers to finish, removing items as soon as they are dispatched / emitted
  // SUPER-IMPORTANT: Subscribing to Data-Events / Going into Flowing-Mode triggers "_read()" to be performed
  reader.on('data', (item: number) => {
    console.log('CONSUMED:', item);

    // SUPER-IMPORTANT: NOT WAITING FOR THE EVENT-HANDLER TO FINISH
    return new Promise<void>((resolve) => {
      // Runs after the Main-Phase is executed completely, since Reading and Event-Emitting runs synchronously
      setImmediate(() => {
        console.log('PROCESSED:', item);

        resolve();
      });
    });
  });

  // Causes reading to be done automatically even without events
  //   - All the items are cleared out as well
  //   -> Items / Internal-Buffer is ALWAYS cleared in Flowing-Mode
  //   => EXACT SAME EFFECT AS SUBSCRIBING TO DATA-EVENTS (Without consuming them)
  // reader.resume();

  // Note: Could Pause and Resume READABLE to control Flow and Back-Pressure better - Done in Piping
};

// Paused-Mode tests
// testPausedMode(TestReader(Date.now()));
// testPausedMode(TestReaderWithSizes(Date.now()));

// Flowing-Mode tests
testFlowingMode(TestReader(Date.now()));
