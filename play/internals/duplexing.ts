import { Duplex } from 'stream';

const TestDuplex = (): Duplex =>
  new Duplex({
    // Reading part
    read() {
      const item = Math.random();

      console.log('Read-Operation:', item);

      this.push(item);
    },

    readableObjectMode: true,
    readableHighWaterMark: 2,

    // Writing part
    // Single-Writing: Used when Input-Internal-Buffer / Write-Internal-Buffer contains just 1 item
    //    - Called in the beginning
    //    - Called AFTER draining
    //    - Called when there is no buffered input-data / write-data
    //    - Called when "end(DATA)" was called with actual data
    write(
      item: number,
      _: BufferEncoding,
      callback: (error?: Error | null) => void,
    ) {
      console.log('Single writing:', item);

      setTimeout(() => {
        callback();
      }, 100);
    },
    // Multiple-Writing: Used when Input-Internal-Buffer / Write-Internal-Buffer contains MORE 1 item
    //    - Called when some data got buffered (AT LEAST 2)
    //    - Called IN ORDER to do DRAINING
    //    - Called when there IS buffered input-data / write-data
    writev(
      items: Array<{ chunk: number; encoding: BufferEncoding }>,
      callback: (error?: Error | null) => void,
    ) {
      console.log(
        'Multiple writing:',
        items.map((it) => it.chunk),
      );

      setTimeout(() => {
        callback();
      }, 100);
    },

    // Finalizing: Called when writing is finished / ended - "end()" is called
    final(callback: (error?: Error | null) => void) {
      console.log('Finished');

      callback();
    },

    writableObjectMode: true,
    writableHighWaterMark: 3,
  });

const testDuplex = (duplex: Duplex) => {
  // Starting read-process ->
  //   1) Calling "_read()" to read X items (Size / Internal logic)
  //   2) Starting calling "_read()" after EACH "this.push(DATA)" in order to fill up Readable-Internal-Buffer
  //        => Reading until HWM is reached (Buffering internally) - Does NOT matter if nothing is read
  console.log('Read-Result:', duplex.read());

  while (duplex.write(Math.random())) {}

  duplex.on('drain', () => {
    // IMPORTANT: Causing Single-Write / "_write()" to be called BEFORE finalizing / calling "_final()"
    duplex.end(Math.random());
  });
};

testDuplex(TestDuplex());
