import { Readable, Transform, TransformCallback } from 'stream';

const testTransformationBackpressure = () => {
  let num = 1;

  const source = new Readable({
    objectMode: true,
    highWaterMark: 2,

    read() {
      console.log('Reading', num, (this as any)._readableState.length);

      this.push(num++);
    },
  });

  const processor = new Transform({
    objectMode: true,
    readableHighWaterMark: 2, // Buffering 2 outputs
    writableHighWaterMark: 3, // Buffering 3 inputs

    transform(val: number, _: BufferEncoding, callback: TransformCallback) {
      console.log(
        'Processing',
        val,
        (this as any)._writableState.length,
        (this as any)._readableState.length,
      );

      setTimeout(() => {
        console.log(
          'Finished processing',
          val,
          (this as any)._writableState.length,
          (this as any)._readableState.length,
        );

        callback(null, val * 10);
      }, 200);
    },
  });

  source.pipe(processor);

  setTimeout(() => {
    console.log('Status');

    console.log((source as any)._readableState.length);

    console.log((processor as any)._writableState.length);
    console.log((processor as any)._readableState.length);
  }, 100);

  setInterval(() => {
    console.log('Tick');
  }, 10);
};

testTransformationBackpressure();
