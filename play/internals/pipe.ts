import { Readable, Writable } from 'stream';

// Important: "pipe()" implementation - Does NOT implement complex cases which are handled by "pipeline()"

// OVERSIMPLIFIED "pipe()" implementation
const usePipe = (readable: Readable, writable: Writable) => {
  readable.on('data', (element: number) => {
    // console.log('Piping:', element);

    const result = writable.write(element, readable.readableEncoding);

    console.log('Pipe-Result:', element, result);

    if (!result) {
      console.log('STOPPING');

      readable.pause();
    }
  });

  readable.on('end', () => {
    writable.end();
  });

  writable.on('drain', () => {
    console.log('RESUMING');

    readable.resume();
  });

  // SUPER-IMPORTANT: Real implementation has much more logic for errors / states / un-piping / etc.

  readable.resume();
};

const main = () => {
  const maxItems = 10;
  const startTime = Date.now();

  let counter = 1;

  const testReader = new Readable({
    // Getting called until LOCAL Internal-Buffer fills up - DOES NOT DEPEND ON Destination(s)
    //     -> LOCAL Internal-Buffers gets cleared up if there are Destinations reading from it
    //     => Passing data from the Internal-Buffer of Sources to the Internal-Buffer of the Destination(s)
    // Pushing: Adding items / data to the LOCAL Internal-Buffer DIRECTLY - Disrespects Internal-Buffer size (HWM)
    //     -> Same as the "this.push()" called inside the "read()"
    //     => "_read()" is the Stream simply ASKING for the next item(s) to be PUSHED respecting Size of Buffer / HWM / Back-Pressure
    // Modes: Data is getting read by "_read()" irrespective of whether the Readable is in Flowing or Paused mode
    //   Flowing: Emitting 'data' event every time an item is PUSHED (Externally / From "_read()")
    //     - An event is EMITTED for each item -> The item is removed from the Internal-Buffer
    //   Paused: NOT emitting any events and simply BUFFERING the items PUSHED to the Internal-Buffer (Storing them)
    //     - Does NOT stop "_read()" from being called -> It is still called until the Internal-Buffer fills up
    // IMPORTANT: Reading into Internal-Buffer - NOT into Writable-Destination(s)
    //     -> Gets 1) Read manually via EXTERNAL "read()" => Read items are removed from the Internal-Buffer afterwards
    //             2) Emitted in a "data" event one-by-one => Emitted items are removed from the Internal-Buffer
    //     => Piping makes use of "data" event to actually write data to the Writable-Destination(s)
    read() {
      // console.log('Reading:', Date.now() - startTime, counter);

      if (counter === maxItems) {
        this.push(null);
      } else {
        this.push(counter++);
      }

      console.log('Read-Buffer:', (this as any)._readableState.length);
    },
    objectMode: true,
    highWaterMark: 3, // Keeping an Internal-Buffer of 3 items -> Stopping calling "read()" when the Buffer becomes FULL
    //  - COMPLETELY SEPARATE FROM the Internal-Buffer of Writable -> Passing data to it from the local internal buffer
    //  => Reading data until the Internal-Buffer fills up (Has NOTHING to do with the Internal-Buffer of Destinations)
  });

  const testWriter = new Writable({
    write(
      element: number,
      _: BufferEncoding,
      callback: (error?: Error | null) => void,
    ) {
      console.log('Write-Buffer:', (this as any)._writableState.length);
      // console.log('Writing:', Date.now() - startTime, element);

      setTimeout(() => {
        console.log('Finished:', Date.now() - startTime, element);

        callback(null);
      }, 500);
    },
    objectMode: true,
    highWaterMark: 2,
  });

  usePipe(testReader, testWriter);

  setInterval(() => {
    console.log('Tick');
  }, 100);
};

main();
