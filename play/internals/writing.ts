import { Writable } from 'stream';

const TestWriter = (start: number): Writable =>
  new Writable({
    write(
      element: number,
      _: BufferEncoding,
      callback: (error?: Error | null) => void,
    ) {
      setTimeout(() => {
        console.log('Output:', element, Date.now() - start);

        // SUPER-IMPORTANT: FIFO - Not writing an element / processing an element using "write()" until the previous element is finished
        //    -> Waiting until the callback is called to process the next element (Buffering them internally until then)
        //    => Taking an element from the internal buffer when the callback is called
        callback(null);
      }, element * 10);
    },

    objectMode: true,
    highWaterMark: 3,
  });

const waitForWriterToBeReady = (writable: Writable): Promise<void> =>
  new Promise<void>((resolve) => {
    writable.on('drain', () => {
      resolve();
    });
  });

const main = async () => {
  const writer = TestWriter(Date.now());

  const elements = [5, 1, 11, 0, 2, 8, 10, 7, 4, 9, 6, 3];

  for (const element of elements) {
    const result = writer.write(element);

    console.log('Input:', element, result);

    if (!result) {
      await waitForWriterToBeReady(writer);
    }
  }
};

main()
  .then(() => {
    console.log('Finished');
  })
  .catch((error: any) => {
    console.log('Failed', error);
  });
