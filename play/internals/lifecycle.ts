import { Writable } from 'stream';

const Writer = (delay: number, hwm?: number): Writable => {
  const data: number[] = [];

  return new Writable({
    // ----------------------------------------------------------------------------------------------------------------
    // Lifecycle
    // ----------------------------------------------------------------------------------------------------------------
    // Initializing -> Setting up State / Resource => Allows asynchronous setup (Callback) - DEPRECATED
    // Default: No logic / functionality
    // construct(callback: (error?: Error | null) => void) {
    //   console.log('Construction');
    //
    //   data = [];
    //
    //   callback();
    // },
    // Finishing -> Cleaning up resource and tearing down Stream / Destination (1st-Phase / Close)
    // Default: Resolving Callback -> Triggering Destroy with / without error (If settings specify to do so)
    // Event: "finish"
    // Important: Only called when "end()" was called to finish the Writable
    //    - NOT when the Writable was closed due to Error
    //    - NOT when the Writable was destroyed with "destroy()"
    final(callback: (error?: Error | null) => void) {
      console.log('Finishing');

      if (data.length === 0) {
        callback(new Error('No data'));

        this.destroy(new Error('No data'));
      } else {
        callback();

        this.destroy(); // HAVING TO TRIGGER MANUALLY
      }
    },
    // Clean up -> Closing the resource COMPLETELY (2nd-Phase / Post-Close)
    // Default: Resolving Callback ONLY (PASSING ERROR ALONG)
    // Event: "close"
    // Note: Can be triggered externally by "destroy()" - Forcibly closing
    destroy(error: Error | null, callback: (error: Error | null) => void) {
      console.log('Destroying:', error);

      data.splice(0, data.length - 1);

      callback(error); // IMPORTANT: "error" is ONLY called when ERROR is passed along
    },

    // ----------------------------------------------------------------------------------------------------------------
    // Writing
    // ----------------------------------------------------------------------------------------------------------------
    write(
      value: number,
      _: BufferEncoding,
      callback: (error?: Error | null) => void,
    ) {
      console.log('Writing ONE');

      setTimeout(() => {
        if (value < 0) {
          callback(new Error('FAILURE'));
        }

        data.push(value);

        console.log('Written:', value, data);

        callback(null);
      }, delay);
    },
    writev(
      input: Array<{ chunk: number; encoding: BufferEncoding }>,
      callback: (error?: Error | null) => void,
    ) {
      console.log('Writing MANY');

      const values = input.map(
        (input: { chunk: any; encoding: BufferEncoding }) => input.chunk,
      );

      setTimeout(() => {
        for (const value of values) {
          if (value < 0) {
            callback(new Error('FAILURE'));

            return;
          }

          data.push(value);
        }

        console.log('Written:', values, data);

        callback(null);
      }, delay);
    },

    // ----------------------------------------------------------------------------------------------------------------
    // Settings
    // ----------------------------------------------------------------------------------------------------------------
    objectMode: true,
    highWaterMark: hwm || 100,
    // emitClose: true, // Instructing whether "close" needs to be called AFTER destroying or NOT
    // autoDestroy: true, // Destroying automatically when finishing / closing
  });
};

const testWritingOne = () => {
  const writer = Writer(0);

  // NOT EMITTED: ONLY EMITTING WHEN WRITER ENTERED DRAINING-MODE
  //     - After Internal-Buffer was filled and FALSE was returning from "write()"
  writer.on('drain', () => {
    console.log('DRAINED');
  });

  // Called FIRST: Ending / Finishing / "final()" - Output ended / Data written
  writer.on('finish', () => {
    console.log('FINISHED');
  });

  // Called SECOND: Destroying / "destroy()" - Resource closed / destroyed
  writer.on('close', () => {
    console.log('CLOSED');
  });

  // Note: NO EVENT FOR DESTROYING

  const result = writer.write(1, (error: Error | null) => {
    console.log('Resolved writing', error);

    writer.end(() => {
      console.log('Resolved ending');
    });
  });

  console.log('Result:', result);
};

const testWritingMany = () => {
  const writer = Writer(10, 5);

  writer.on('drain', () => {
    console.log('DRAINED THE BUFFER');

    writer.end(100, () => {
      console.log('FINISHED');
    });
  });

  writer.on('close', () => {
    console.log('RESOURCE IS CLOSED');
  });

  // Writes:
  //   1) Single-Write for Initial-Write (Buffer is empty)
  //   2) Many-Write with ALL the Buffered Items
  //        - Called after the Initial-Write is finished
  //        -> Emitting "drain" event (Due to draining finishing and Buffer being emptied)
  for (let i = 0; i < 10; i++) {
    if (!writer.write(i)) {
      break;
    }
  }
};

const testWritingWithError = () => {
  const writer = Writer(50);

  // Closing Flow: (Success: final() -> "finish") -> destroy() -> (Failure: "error") -> "close"
  // Important: Error could be caused by 1) Writing = "_write()" / "_writev()"
  //                                     2) Finishing = "_final()"
  //    -> Same error in BOTH cases
  // SUPER-IMPORTANT: DESTROY IS CALLED BEFORE ERROR IS EMITTED -> Can handle it!

  // Error-Callback: Invoked AFTER "destroy" event
  //    - Depending on whether Error was passed along by the Destruction or not
  writer.on('error', (error: Error) => {
    console.log('ERRORED', error);

    // Has NO effect - NOT calling a corresponding "_write()"
    writer.write(100);
  });

  // NOT CALLED: Only when finishing with NO ERRORS
  writer.on('finish', () => {
    console.log('FINISHED');
  });

  // ALWAYS CALLED AFTER DESTRUCTION / DESTROYING
  writer.on('close', () => {
    console.log('CLOSED');
  });

  const Callback = (label: number) => (error?: Error) => {
    console.log('AWAITED', label, error);
  };

  writer.write(1, Callback(1));
  writer.write(2, Callback(2));
  writer.write(3, Callback(3));
  writer.write(-1, Callback(-1));
  writer.end(); // HAS NO EFFECT due to the Stream closing anyway
};

const testWritingFinishingWithError = () => {
  const writer = Writer(0);

  writer.on('error', (error: Error) => {
    console.log('ERRORED', error);
  });

  writer.on('finish', () => {
    console.log('FINISHED');
  });

  writer.on('close', () => {
    console.log('CLOSED');
  });

  writer.write(1);

  writer.end(); // Causing an error due to no items written
};

const testWritingWithDestroying = () => {
  const writer = Writer(50);

  writer.on('finish', () => {
    console.log('FINISHED');
  });

  writer.on('close', () => {
    console.log('CLOSED');
  });

  writer.write(1, () => {
    writer.destroy();
  });
  writer.write(2);
  writer.write(3);
};

testWritingFinishingWithError();
