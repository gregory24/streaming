import { Readable } from 'stream';
import {read} from 'fs';

export type IInputSourceWriter<InputType extends any> = (
  input: InputType,
) => void;

export type IInputSource<InputType extends any> = [
  IInputSourceWriter<InputType>,
  Readable,
];

export type IInputSourceConfiguration<
  InputType extends any,
  OutputType extends any,
> = Readonly<{
  max: number;
  isObject: boolean;
  factory: (input: InputType) => OutputType;
}>;

const DEFAULT_INPUT_SOURCE_CONFIGURATION: IInputSourceConfiguration<any, any> =
  {
    max: 10,
    isObject: true,
    factory: (v: any) => v,
  };

// Automatic-Backpressure implementation

export const InputSource = <
  InputType extends any = any,
  OutputType extends any = any,
>(
  config: Partial<IInputSourceConfiguration<InputType, OutputType>>,
): IInputSource<InputType> => {
  const { max, isObject, factory } = {
    ...DEFAULT_INPUT_SOURCE_CONFIGURATION,
    ...config,
  };

  const readable: Readable = new Readable({
    highWaterMark: max,
    objectMode: isObject,
  });

  const writer: IInputSourceWriter<InputType> = (input: InputType) => {
    readable.push(factory(input));
  };

  return [writer, readable];
};

// Manual-Backpressure implementation

export const InputSourceWithBuffer = <
  InputType extends any = any,
  OutputType extends any = any,
>(
  config: Partial<IInputSourceConfiguration<InputType, OutputType>>,
): IInputSource<InputType> => {
  const { max, isObject, factory } = {
    ...DEFAULT_INPUT_SOURCE_CONFIGURATION,
    ...config,
  };

  const buffer: InputType[] = [];

  const writer: IInputSourceWriter<InputType> = (input: InputType) => {
    buffer.push(input);
  };

  const readable: Readable = new Readable({
    read(size: number) {
      const elements = buffer.splice(0, size);

      for (const element of elements) {
        this.push(factory(element));
      }
    },

    highWaterMark: max,
    objectMode: isObject,
  });

  return [writer, readable];
};
